﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)

        {

            double[] KitNumbers = { 12, 3, 1, 33, 44, 27, 23, 7, 17, 9, 11, 55, 19 };       
            NumberSequence KitNumbersSequentialSorted = new NumberSequence(KitNumbers);
            NumberSequence KitNumbersBubbleSorted = new NumberSequence(KitNumbers);
            NumberSequence KitNumbersCombSorted = new NumberSequence(KitNumbers);
            
            SortStrategy SequentialSort = new SequentialSort();
            SortStrategy BubbleSort = new BubbleSort();
            SortStrategy CombSort = new CombSort();

            KitNumbersSequentialSorted.SetSortStrategy(SequentialSort);
            KitNumbersBubbleSorted.SetSortStrategy(BubbleSort);
            KitNumbersCombSorted.SetSortStrategy(CombSort);
            KitNumbersSequentialSorted.Sort();
            KitNumbersBubbleSorted.Sort();
            KitNumbersCombSorted.Sort();

            Console.WriteLine("SequentialSort" + KitNumbersSequentialSorted.ToString());
            Console.WriteLine("BubbleSort" + KitNumbersBubbleSorted.ToString());
            Console.WriteLine("CombSort" + KitNumbersCombSorted.ToString());




        }
    }
}
