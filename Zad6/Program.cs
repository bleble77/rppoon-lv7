﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad6
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem book = new Book("Bible", 27);
            IItem vhs = new VHS("Wedding of prince Charles and Lady Diane", 20);
            IItem dvd = new DVD("Chronicles of Narnia", DVDType.MOVIE, 25);
            IItem software = new DVD("Antivirus", DVDType.SOFTWARE, 17);
            IVisitor Renter = new RentVisitor();

            Console.WriteLine(book.ToString());
            Console.WriteLine(book.Accept(Renter));

            Console.WriteLine(vhs.ToString());
            Console.WriteLine(vhs.Accept(Renter));
            
            Console.WriteLine(dvd.ToString());
            Console.WriteLine(dvd.Accept(Renter));
            
            Console.WriteLine(software.ToString());
            Console.WriteLine(software.Accept(Renter));
        }
    }
}
