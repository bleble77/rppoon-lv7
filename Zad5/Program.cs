﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            IItem book = new Book("Bible", 27);
            IItem vhs = new VHS("Wedding of prince Charles and Lady Diane", 20);
            IItem dvd = new DVD("Chronicles of Narnia", DVDType.MOVIE, 25);
            IVisitor Buyer = new BuyVisitor();

            Console.WriteLine(book.ToString());
            Console.WriteLine(book.Accept(Buyer));

            Console.WriteLine(vhs.ToString());
            Console.WriteLine(vhs.Accept(Buyer));

            Console.WriteLine(dvd.ToString());
            Console.WriteLine(dvd.Accept(Buyer));
        }
    }
}
