﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            double SearchNumber1 = 55;
            double SearchNumber2 = 88;
            double[] Array = { 1, 2, 3, 458, 8, 2, 88, 23, 9, 4, 6, 8, 233, 12, 19, 17 };
            NumberSequence numberSequence = new NumberSequence(Array);
            SearchStrategy LinearSearch = new LinearSearch();
            numberSequence.SetSearchStrategy(LinearSearch);
            bool Result1 = numberSequence.Search(SearchNumber1);
            if (Result1 == true)
                Console.WriteLine(SearchNumber1 + " has been found in array\n");
            else
                Console.WriteLine(SearchNumber1 + "  has not been found in array\n");

            bool Result2 = numberSequence.Search(SearchNumber2);
            if (Result2 == true)
                Console.WriteLine(SearchNumber2 + " has been found in array\n");
            else
                Console.WriteLine(SearchNumber2 + "  has not been found in array\n");

        }
    }
}
