﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class LinearSearch : SearchStrategy
    {
        public override bool Search(double[] array, double number)
        {
            int ArraySize = array.Length;
            for (int i = 0; i < ArraySize; i++)
            {
                if (array[i] == number)
                    return true;
                
            }
            return false;
        }
    }
}
